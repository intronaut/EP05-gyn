## EP05Av-GYN dag5

voor degenen die geen markdown syntax aankunnen


## PR patientroute 2.0 - elearning voorbereiding

- behandelopties endometriose
    - ablatio endometriose
    - zilvernitraat applicatie
    - radicaal chirurgisch

### Vraag 6

Jouw patiënte heeft naast pijnklachten ook verminderde vruchtbaarheid. Dit kan ook door de endometriose komen, op basis van welk mechanisme gebeurt dit?

- bloedingen in laesies, die gemakkelijker ontstaat in het geval van endometriose, en verlittekening met ijzerpigment-neerslag die daar secundair bij horen => kunnen leiden tot adhesies in het kleine bekken en verstoring vd pelviene anatomie => gevolg is dat er zich endometriomen in de ovaria bevinden. => leidt tot subfertility

### Vraag 2

Aan de vrouw stel je vragen om afwijkingen te kunnen opsporen, waaronder een cyclusstoornis, endocriene stoornis en stoornis uit eerdere zwangerschappen.

Welke vragen kan je aan de vrouw stellen om een cyclusstoornis op te sporen?
- menarche, regelmaat, sexuele activiteit, mogelijkheid tot zwangerschap, klachten peri-menstrueel, abnormaliteiten bloedverlies


Welke vragen kan je aan de vrouw stellen om een endocriene stoornis op het spoor te komen?


- puberteitsontwikkeling => start, sec. sex kenmerken, 
CAVE:  PCOS bij opvliegers op jonge leeftijd  ; sterke gewichtsverandering ; hypothalaam = intensief sporten


Vraag 3

Er zijn ook een aantal ziektes die kunnen leiden tot verminderde vruchtbaarheid bij mannen. Welke van die ziektes ken jij?

- boforchitis post-puberteit
- trauma testis / torsio testis
- maldescensus testis (al dan niet hormonaal of chirurgisch behandeld)
- behadneling met radioTx, cytostatica of salazosulfapyridine
- aanwezigheid algemene ziekten:  recidiv bronchitis, CF
- chromosomale afwijking
- CBAVD (aangeboren)

- 3 uitzonderingen ;; vrijwel is er nooit een causale behandleing, behalve ingeval van:
  + (partieel) hypogonad hypogonadism (< 1% van mannen met fertilitetisstoornis) ; behandeling is pulsatiel GnRH
  + hyperprolactinaemie:  nagenoeg altijd ihkv prolactinoom (prolactine-producerend hypofyseadenoom), bijna 
altijd gepaard met libidostoornis
  + obstructie ter hoogte vd epididymis ; meestal na doorgemaakte epididdymitis , soms blijkt man last gehad te hebben van recidiv cystitiden of SOA

- uiteraard talloze hormoonbehandelingen ihkv farmacologische behandeling waar dan ook, kunnen leiden tot subfertiliteit/infertiliteit bij de man



## PR patientroute 2.0 - beelden/foto beoordelen

> de nummers referen naar de nummers vd afbeeldingen

- \10. gardnerella
    + overschot aan commensale gisten in vagina
    + veel voorkomend
- \02. bartholinitis (= niet hetzelfde als een Bartho cyst)
    + afvoerkathether die vanuit de Barth-itis  naar buiten loopt; enkele weken laten in situ laten, totdat het gebied rustig is, waarna in principe de Barth-itis regresseert.
    + alternatief voor marsuplialisation?
- \05. HPV (crappy ass plaatje van een virus)
    + diagnostiek
    + HPV 6 + 11
    + anogen wratten (AGW()
    + contactonderzoek en partnerwaarschuwing
    + overdracht kan door asymptomatisch drager
- \13. Lichen sclerosis
    + diagnose a vue ; biopten nemen van lichen sclerosis zijn in principe verlatenin de kliniek
    + kan echter - zeker indien onbehandeld - progresseren tot maligniteit
- \03. CIN
    + premaligne stadia ihkv cervixca
    + er worden aankleuringen (o.a. lugol) gebruikt ter localisatie van mogelijke CIN-laesies op  ; eerst donkerkleurig (mbv lugol), vervolgens aangekleurd met een lichtere kleur => CIN-laesies worden zichtbaar met het blote oog en op die manier kan je zien waar je biopten moet nemen. 
- \04. Endometriose (met adhaesies in tubae)
    + hysteroscopie met contrast => ter beoordeling doorgankelijkheid tuba ihkv kinderwens
- \07. Endometriose
    + kan leiden tot vorming van cysten, waarbij ook fertiliteits/conceptiekans-vermindering is.
- \14. Miskraam
    + afbeelding zonder context zegt niets over wat je ziet
    + een echo met alleen maar vocht bij zw.schap duur van 4wk, zelfs zonder dooierzak, kan volstrekt normaal zijn
    +  idem, maar bij 8wk zw.schap duur, zou dit wel degelijk een afwijkende echo zijn; nl. zijnde een non-vitale zwangerschap (ookwel: missed abortion)
- \09. EUG
    + EUG slechts te herkennen adhv de foto ; de EUG ttv de foto al verwijdert
    + de aanwijzing was het zgn hevige bloedverlies met hemostase in Cavum Douglasi
- \17. PID
    + blijkbaar kweek tbv diagnostiek
    + therapie is langdurig antibiotica ; kweek inzetten tbv resistentieprofiel
- \01. Torsio adnexis
    + self-explanatory
    + zichtbare torsie, met stuwing, oedeem, hypoperfusie-aspect
- \11. IUD
    + een IUD, gefotografeerd door een talentloze fotograaf
    + indien er dislocatie is, ingroeiing in de uteruswand, of indien de draden niet meer bereikbaar zijn ttv geplande verwijdering IUD => hystoscopie om het IUD in situ in beeld te krijgen
- \00. intracavitaire afwijking polyp/myoom
    + locatie in uterus: bloedingsklachten meest prominent
    + locatie buiten uterus: micite/obstipatieklachten
- \19. Tumor onderbuik
    + self-explanatory huge mass in abdomen
    + Therapie (invasief):
        * embolisatie
        * enucleatie myoom
        * uterusextirpatie
- \18. Totaal prolaps
    + self-explanatory ; in dit geval zo goed als diagnose a vue
    + meest voorkomend in post-menopauzale leeftijd
    + classificatie volgens POP-Q: stage 0-IV afhankelijk van ernst/uitgebreidheid vd prolaps (tot welk niveau het prolabeert)
- \06. Pressarium
    + na een partus wordt de kans op onwikkeling van prolaps inherent groter
    + pressarium is hierbij meestal het eerste hulpmiddel van keuze
- \16. ovariumca
    + vaak laat ontdekt itt endometriumca
    + ovariumca geeft vage klachten: buikpijn, misselijk, opgezette buik => hierdoor vaak al metastatisch bij first presentation
    + **CAVE:** iedereen met ovariumca krijgt genetisch onderzoek, gezien groot aantal genetische substraten te vinden zijn mbt hoog-risico-situaties en erfelijkheid.
- \15. mola zwangerschap / teratoom
    + klassiek een echografisch beeld van een 'druiventros' ; dit beeld wordt tegenwoordig echter niet meer gezien in de kliniek
    + therapie:  verwijdering van al het mola-materiaal
- \08. Pipelle
    + hulpmiddel dat via vagina, door portio/cervix, ingebracht wordt in uteri. Gebruikt voor het nemen van een biopt ter beoordeling van eventueel aanwezig endometrium carcinoom
- \20. Sentinel node bij vulvacarcinoom
    + foto indiceert uberhaupt geen localisatie bij de vulva
    + vroeger werden volledige liesklierdissecties gedaan bij behandeling van vulvaca ; tegenwoordig is er een sentinel node procedure, met veel beter resultaat tav restafwijkingen/bijwerkingen/restklachten
